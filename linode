#!/bin/bash -e
SCRIPT_DIR="$(cd -P "$(dirname "$0")" && pwd)"

SSH_CONFIG_OPTS=(-o BatchMode=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null)

die() { echo "$1" >&2; exit 1; }
cmd() { command -v "$1" || die "$1 not found"; }

CURL="$(cmd curl)"
JQ="$(cmd jq)"
SSH="$(cmd ssh)"
SSH_KEYGEN="$(cmd ssh-keygen)"

linode_api() { "$SCRIPT_DIR/linode-api" "$@"; }
jq() { $JQ -r "$@"; }

function randstr {
  LC_CTYPE=C tr -dc a-zA-Z0-9\ \!\"\'\(\)\,\\-\.\/\:\;\<\>\?\[\]\`\{\} </dev/urandom \
    | fold -w "${1-32}" \
    | head -n "${2-1}"
}

function sshcmd {
  local SSH_OPTS=("${SSH_CONFIG_OPTS[@]}")

  while [ $# -gt 2 ]; do
    SSH_OPTS+=("$1"); shift
  done

  $SSH -q "${SSH_OPTS[@]}" "root@$1" "$2"
}

LINODE_ID=
LABEL=
REGION=us-east
AZ=
DATACENTER_ID=
SIZE=1024
PLAN_ID=
SWAP_SIZE=1024
HOSTNAME=
DOMAIN=
FQDN=
COREOS_VERSION=stable
COREOS_CONFIG_FILE="$SCRIPT_DIR/coreos-config.yml"
SWAPPINESS=
DNS="8.8.8.8 8.8.4.4"
SEARCH_DOMAINS=
SSH_KEY_FILE="$HOME/.ssh/id_rsa.pub"
SSH_KEY=
SSH_INSTALL_IDENTITY="$SCRIPT_DIR/install.key"
SSH_INSTALL_KEY_FILE="$SCRIPT_DIR/install.key.pub"
SSH_INSTALL_KEY=

USAGE="Usage: $0 [-cCdDhklnrsSw]"

while getopts c:C:d:D:h:k:l:n:r:s:S:w: OPTION; do
  case $OPTION in
    l) LABEL="$OPTARG" ;;
    r) REGION="$OPTARG" ;;
    s) SIZE="$OPTARG" ;;
    S) SWAP_SIZE="$OPTARG" ;;
    h) HOSTNAME="$OPTARG" ;;
    d) DOMAIN="$OPTARG" ;;
    C) COREOS_VERSION="$OPTARG" ;;
    c) COREOS_CONFIG_FILE="$OPTARG" ;;
    w) SWAPPINESS="$OPTARG" ;;
    n) DNS="$OPTARG" ;;
    D) SEARCH_DOMAINS="$OPTARG" ;;
    k) SSH_KEY_FILE="$OPTARG" ;;
  esac
done; shift $((OPTIND -1))

if [ -n "$REGION" ]; then
  case "$REGION" in
    us-east)
      AZ=$REGION-1a
      DATACENTER_ID=6
      ;;
    us-southeast)
      AZ=$REGION-1a
      DATACENTER_ID=4
      ;;
  esac
fi

if [ -z "$LABEL" -a -n "$AZ" -a -n "$HOSTNAME" ]; then
  LABEL="$AZ-$HOSTNAME"
fi

if [ -n "$SIZE" ]; then
  case "$SIZE" in
    1024) PLAN_ID=1 ;;
    2048) PLAN_ID=2 ;;
    4096) PLAN_ID=3 ;;
  esac

  if [ -z "$SWAPPINESS" ]; then
    case "$SIZE" in
      1024) SWAPPINESS=60 ;;
      2048) SWAPPINESS=60 ;;
      4096) SWAPPINESS=40 ;;
    esac
  fi
fi

if [ -n "$HOSTNAME" -a -n "$DOMAIN" ]; then
  FQDN="$HOSTNAME.$DOMAIN"
fi

if [ -z "$SEARCH_DOMAINS" ]; then
  SEARCH_DOMAINS="members.linode.com"

  if [ -n "$DOMAIN" ]; then
    SEARCH_DOMAINS="$DOMAIN $SEARCH_DOMAINS"
  fi
fi

if [ -n "$SSH_KEY_FILE" -a -r "$SSH_KEY_FILE" ]; then
  SSH_KEY="$(cat "$SSH_KEY_FILE")"
fi

if [ -z "$LABEL" ] \
  || [ -z "$REGION" -o -z "$AZ" -o -z "$DATACENTER_ID" ] \
  || [ -z "$SIZE" -o -z "$PLAN_ID" -o -z "$SWAP_SIZE" ] \
  || [ -z "$HOSTNAME" -o -z "$DOMAIN" ] \
  || [ -z "$COREOS_VERSION" -o -z "$COREOS_CONFIG_FILE" ] || [ ! -r "$COREOS_CONFIG_FILE" ] \
  || [ -z "$SWAPPINESS" -o -z "$DNS" -o -z "$SEARCH_DOMAINS" -o -z "$SSH_KEY_FILE" -o -z "$SSH_KEY" ]; then
  die "$USAGE"
fi

if [ ! -f "$SSH_INSTALL_IDENTITY" ] || [ ! -f "$SSH_INSTALL_KEY_FILE" ]; then
  rm -f "$SSH_INSTALL_IDENTITY" "$SSH_INSTALL_KEY_FILE"
  $SSH_KEYGEN -q -t rsa -N "" -C "install" -f "$SSH_INSTALL_IDENTITY"
fi

SSH_INSTALL_KEY="$(cat "$SSH_INSTALL_KEY_FILE")"

LINODE_ID="$(linode_api linode.list | jq "map(select(.LABEL==\"$LABEL\")) | first | .LINODEID")"

if [ "$LINODE_ID" == null ]; then
  echo -n "Creating new Linode '$LABEL'..."

  LINODE_ID="$(linode_api linode.create "DatacenterID=$DATACENTER_ID" "PlanID=$PLAN_ID" | jq '.LinodeID')"

  echo -n " waiting..."

  sleep 3

  while [ "$(linode_api linode.list "LinodeID=$LINODE_ID" | jq 'first | .STATUS')" != "0" ]; do
    sleep 3
  done

  linode_api linode.update "LinodeID=$LINODE_ID" "Label=$LABEL" >/dev/null 2>&1

  echo " done."

  sleep 1

  echo -n "Gathering details..."
else
  echo -n "Gathering details on '$LABEL'..."
fi

DISK_SPACE="$(linode_api linode.list "LinodeID=$LINODE_ID" | jq 'first | .TOTALHD')"
IPS="$(linode_api linode.ip.list "LinodeID=$LINODE_ID")"
IPV4_PUBLIC="$(jq 'map(select(.ISPUBLIC==1)) | first | .IPADDRESS' <<<"$IPS")"
IPV4_GATEWAY="$(printf '%d.%d.%d.1' $(echo "$IPV4_PUBLIC" | cut -d . -f 1-3 | sed 's/\./ /g'))"
IPV4_PRIVATE="$(jq 'map(select(.ISPUBLIC==0)) | first | .IPADDRESS' <<<"$IPS")"

echo " done."

if [ "$IPV4_PRIVATE" == null ]; then
  sleep 1

  echo -n "Adding private IPv4..."
  IPV4_PRIVATE="$(linode_api linode.ip.addprivate "LinodeID=$LINODE_ID" | jq '.IPADDRESS')"
  echo " done."
fi

sleep 1

DOMAIN_ID="$(linode_api domain.list | jq "map(select(.DOMAIN==\"$DOMAIN\")) | first | .DOMAINID")"

if [ -n "$DOMAIN_ID" -a "$DOMAIN_ID" != null ]; then
  RESOURCES="$(linode_api domain.resource.list "DomainID=$DOMAIN_ID" | jq "map(select(.TYPE==\"A\" and .NAME==\"$HOSTNAME\"))")"
  BAD_RESOURCES="$(jq "map(select(.TARGET!=\"$IPV4_PUBLIC\"))" <<<"$RESOURCES")"

  if [ $(jq length <<<"$BAD_RESOURCES") -gt 0 ]; then
    echo -n "Deleting conflicting DNS records..."

    while read RESOURCE_ID; do
      linode_api domain.resource.delete "DomainID=$DOMAIN_ID" "ResourceID=$RESOURCE_ID" >/dev/null 2>&1
    done <<<"$(jq '.[] | .RESOURCEID' <<<"$BAD_RESOURCES")"

    echo " done."
  fi

  if [ "$(jq "map(select(.TARGET==\"$IPV4_PUBLIC\")) | length" <<<"$RESOURCES")" == "0" ]; then
    echo -n "Adding DNS record..."
    linode_api domain.resource.create "DomainID=$DOMAIN_ID" "Type=A" "Name=$HOSTNAME" "Target=$IPV4_PUBLIC" >/dev/null 2>&1
    echo " done."
  fi
fi

sleep 1

if [ "$(linode_api linode.list "LinodeID=$LINODE_ID" | jq 'first | .STATUS')" == "1" ]; then
  echo -n "Shutting down..."

  JOB_ID="$(linode_api linode.shutdown "LinodeID=$LINODE_ID" | jq '.JobID')"

  echo -n " waiting..."

  sleep 3

  while [ "$(linode_api linode.job.list "LinodeID=$LINODE_ID" "JobID=$JOB_ID" "pendingOnly=1" | jq length)" != "0" ]; do
    sleep 3
  done

  echo " done."
fi

sleep 1

CONFIGS="$(linode_api linode.config.list "LinodeID=$LINODE_ID")"
if [ "$(jq length <<<"$CONFIGS")" != "0" ]; then
  echo -n "Deleting existing boot configs..."

  while read CONFIG_ID; do
    linode_api linode.config.delete "LinodeID=$LINODE_ID" "ConfigID=$CONFIG_ID" >/dev/null 2>&1
  done <<<"$(jq '.[] | .ConfigID' <<<"$CONFIGS")"

  echo " done."
fi

sleep 1

DISKS="$(linode_api linode.disk.list "LinodeID=$LINODE_ID")"
if [ "$(jq length <<<"$DISKS")" != "0" ]; then
  echo -n "Deleting existing disks..."

  JOB_IDS=()

  while read DISK_ID; do
    JOB_IDS+=("$(linode_api linode.disk.delete "LinodeID=$LINODE_ID" "DiskID=$DISK_ID" | jq '.JobID')")
  done <<<"$(jq '.[] | .DISKID' <<<"$DISKS")"

  echo -n " waiting..."

  sleep 3

  JOB_IDS="$(IFS=,;echo "${JOB_IDS[*]}")"

  while [ "$(linode_api linode.job.list "LinodeID=$LINODE_ID" "pendingOnly=1" | jq "map(select(.JOBID==[$JOB_IDS][])) | length")" != "0" ]; do
    sleep 3
  done

  echo " done."

  sleep 1

  echo -n "Creating new disks..."
else
  echo -n "Creating disks..."
fi

JOB_IDS=()

DATA="$(linode_api linode.disk.create "LinodeID=$LINODE_ID" "Label=root" "Type=raw" "Size=$(($DISK_SPACE - $SWAP_SIZE - 1100))")"
ROOT_DISK_ID="$(jq '.DiskID' <<<"$DATA")"
JOB_IDS+=("$(jq '.JobID' <<<"$DATA")")

DATA="$(linode_api linode.disk.create "LinodeID=$LINODE_ID" "Label=swap" "Type=swap" "Size=$SWAP_SIZE")"
SWAP_DISK_ID="$(jq '.DiskID' <<<"$DATA")"
JOB_IDS+=("$(jq '.JobID' <<<"$DATA")")

DATA="$(linode_api linode.disk.createfromdistribution "LinodeID=$LINODE_ID" "DistributionID=158" "Label=install" "Size=1100" "rootPass=$(randstr)" "rootSSHKey=$SSH_INSTALL_KEY")"
INSTALL_DISK_ID="$(jq '.DiskID' <<<"$DATA")"
JOB_IDS+=("$(jq '.JobID' <<<"$DATA")")

echo -n " waiting..."

sleep 3

JOB_IDS="$(IFS=,;echo "${JOB_IDS[*]}")"

while [ "$(linode_api linode.job.list "LinodeID=$LINODE_ID" "pendingOnly=1" | jq "map(select(.JOBID==[$JOB_IDS][])) | length")" != "0" ]; do
  sleep 3
done

echo " done."

sleep 1

echo -n "Creating boot configs..."

BOOT_CONFIG_ID="$(linode_api linode.config.create "LinodeID=$LINODE_ID" "Label=boot" "KernelID=213" "DiskList=$ROOT_DISK_ID,$SWAP_DISK_ID" | jq '.ConfigID')"
linode_api linode.config.update "LinodeID=$LINODE_ID" "ConfigID=$BOOT_CONFIG_ID" "helper_distro=0" "helper_disableUpdateDB=0" "helper_depmod=0" "devtmpfs_automount=0" "helper_network=0" >/dev/null 2>&1

INSTALL_CONFIG_ID="$(linode_api linode.config.create "LinodeID=$LINODE_ID" "Label=install" "KernelID=138" "DiskList=$ROOT_DISK_ID,$INSTALL_DISK_ID" "RootDeviceNum=2" | jq '.ConfigID')"

echo " done."

sleep 1

echo -n "Booting for install..."

JOB_ID="$(linode_api linode.boot "LinodeID=$LINODE_ID" "ConfigID=$INSTALL_CONFIG_ID" | jq '.JobID')"

echo -n " waiting..."

sleep 3

while [ "$(linode_api linode.job.list "LinodeID=$LINODE_ID" "JobID=$JOB_ID" "pendingOnly=1" | jq length)" != "0" ]; do
  sleep 3
done

sleep 3

echo -n " checking..."

while [ "$(sshcmd -i "$SSH_INSTALL_IDENTITY" -o ConnectTimeout=3 "$IPV4_PUBLIC" 'echo up')" != "up" ]; do
  sleep 3
done

echo " done."

sleep 1

echo -n "Installing CoreOS..."

COREOS_INSTALL_URL="https://raw.githubusercontent.com/coreos/init/master/bin/coreos-install"
CT_URL="$($CURL -s "https://api.github.com/repos/coreos/container-linux-config-transpiler/releases/latest" | jq '.assets | map(select(.name | endswith("x86_64-unknown-linux-gnu"))) | first | .browser_download_url')"

sshcmd -i "$SSH_INSTALL_IDENTITY" "$IPV4_PUBLIC" "bash -s -e" <<EOS
curl -s -L -o /usr/local/bin/coreos-install -o /usr/local/bin/ct "$COREOS_INSTALL_URL" "$CT_URL"
chmod +x /usr/local/bin/coreos-install /usr/local/bin/ct
EOS

{
  cat "$COREOS_CONFIG_FILE" | sed \
    -e "s/#{REGION}/$REGION/g" \
    -e "s/#{AZ}/$AZ/g" \
    -e "s/#{HOSTNAME}/$HOSTNAME/g" \
    -e "s/#{DOMAIN}/$DOMAIN/g" \
    -e "s/#{FQDN}/$FQDN/g" \
    -e "s/#{IPV4_PUBLIC}/$IPV4_PUBLIC/g" \
    -e "s/#{IPV4_GATEWAY}/$IPV4_GATEWAY/g" \
    -e "s/#{IPV4_PRIVATE}/$IPV4_PRIVATE/g" \
    -e "s/#{DNS}/$DNS/g" \
    -e "s/#{SEARCH_DOMAINS}/$SEARCH_DOMAINS/g" \
    -e "s/#{SWAPPINESS}/$SWAPPINESS/g" \
    -e "s/#{SSH_KEY}/${SSH_KEY//\//\\/}/g"
} | sshcmd -i "$SSH_INSTALL_IDENTITY" "$IPV4_PUBLIC" "ct -out-file ignition.json"

echo
echo
sshcmd -i "$SSH_INSTALL_IDENTITY" "$IPV4_PUBLIC" "coreos-install -d /dev/sda -C $COREOS_VERSION -i ignition.json"
echo

sleep 1

echo -n "Shutting down..."

JOB_ID="$(linode_api linode.shutdown "LinodeID=$LINODE_ID" | jq '.JobID')"

echo -n " waiting..."

sleep 3

while [ "$(linode_api linode.job.list "LinodeID=$LINODE_ID" "JobID=$JOB_ID" "pendingOnly=1" | jq length)" != "0" ]; do
  sleep 3
done

echo " done."

sleep 1

echo -n "Cleaning up..."

linode_api linode.config.delete "LinodeID=$LINODE_ID" "ConfigID=$INSTALL_CONFIG_ID" >/dev/null 2>&1

JOB_ID="$(linode_api linode.disk.delete "LinodeID=$LINODE_ID" "DiskID=$INSTALL_DISK_ID" | jq '.JobID')"

echo -n " waiting..."

sleep 3

while [ "$(linode_api linode.job.list "LinodeID=$LINODE_ID" "JobID=$JOB_ID" "pendingOnly=1" | jq length)" != "0" ]; do
  sleep 3
done

echo " done."

sleep 1

echo -n "Resizing root disk..."

JOB_ID="$(linode_api linode.disk.resize "LinodeID=$LINODE_ID" "DiskID=$ROOT_DISK_ID" "Size=$(($DISK_SPACE - $SWAP_SIZE))" | jq '.JobID')"

echo -n " waiting..."

sleep 3

while [ "$(linode_api linode.job.list "LinodeID=$LINODE_ID" "JobID=$JOB_ID" "pendingOnly=1" | jq length)" != "0" ]; do
  sleep 3
done

echo " done."

sleep 1

echo -n "Booting..."

JOB_ID="$(linode_api linode.boot "LinodeID=$LINODE_ID" "ConfigID=$BOOT_CONFIG_ID" | jq '.JobID')"

echo -n " waiting..."

sleep 3

while [ "$(linode_api linode.job.list "LinodeID=$LINODE_ID" "JobID=$JOB_ID" "pendingOnly=1" | jq length)" != "0" ]; do
  sleep 3
done

echo " done."
